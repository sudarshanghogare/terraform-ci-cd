//creating sqs resource and adding permissions for lambda

resource "aws_sqs_queue" "test_queu" {
  name = "multi_od_test"
}

// creating event for sqs with lambda
resource "aws_lambda_event_source_mapping" "event_source_mapping" {
  batch_size       = 1
  event_source_arn = "${aws_sqs_queue.test_queu.arn}"
  enabled          = true
  function_name    = "${aws_lambda_function.test_lambda.arn}"
}

//creating lambda permission for sqs
resource "aws_lambda_permission" "allows_sqs_to_trigger_lambda" {
  statement_id  = "AllowExecutionFromSQS"
  action        = "lambda:InvokeFunction"
  function_name = "${aws_lambda_function.test_lambda.function_name}"
  principal     = "sqs.amazonaws.com"
  source_arn    = "${aws_sqs_queue.test_queu.arn}"
}


//policy for creating sqs and adding permission to execute
resource "aws_sqs_queue_policy" "test" {
  queue_url = aws_sqs_queue.test_queu.id

  policy = <<POLICY
{
  "Version": "2012-10-17",
  "Id": "sqspolicy",
  "Statement": [
    {
      "Sid": "First",
      "Effect": "Allow",
      "Principal": "*",
      "Action": "sqs:*",
      "Resource": "${aws_sqs_queue.test_queu.arn}"
    }
  ]
}
POLICY
}

