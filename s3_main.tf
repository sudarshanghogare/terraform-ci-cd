# Creating s3 resource for lambda function#

resource "aws_s3_bucket" "bucket_od_test2" {
bucket = "odpipelines3test"
acl    = "private"
//role   = aws_iam_role.iam_for_lambda_od.arn //attching role for resource
tags = {
Name        = "od oioeline"
Environment = "test"
}
}

# Adding S3 bucket as trigger to  lambda#
resource "aws_s3_bucket_notification" "aws-lambda-trigger" {
bucket = "${aws_s3_bucket.bucket_od_test2.id}"
lambda_function {
lambda_function_arn = "${aws_lambda_function.test_lambda.arn}"
events              = ["s3:ObjectCreated:*"]
filter_prefix       = "file-prefix"
filter_suffix       = "file-extension"
}
}
resource "aws_lambda_permission" "test" {
statement_id  = "AllowS3Invoke"
action        = "lambda:InvokeFunction"
function_name = "${aws_lambda_function.test_lambda.function_name}"
principal = "s3.amazonaws.com"
source_arn = "arn:aws:s3:::${aws_s3_bucket.bucket_od_test2.id}"
}


