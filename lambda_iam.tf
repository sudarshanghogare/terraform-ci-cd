# iam role for lambda #

resource "aws_iam_role" "iam_for_lambda_od" {
  name = "iam_for_lambda_test"
  

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",


      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}

EOF
}


//policy for for resources to give access
resource "aws_iam_policy" "policy-for-access" {
  name        = "sud"
  description = "test"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "ec2:Describe*",
        "s3:*Object",
        "redshift:*",
        "logs:*",
        "cloudformation:*",
        "lambda:*",
        "sns:*",
        "sqs:*"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "attchment_for_policy_and_role" {
  role       = aws_iam_role.iam_for_lambda_od.name
  policy_arn = aws_iam_policy.policy-for-access.arn
}

