// creating step function

/*
resource "aws_sfn_state_machine" "test_multi_of_pipeline" {
    name = "multi_test_od"
    role_arn = aws_iam_role.iam_for_lambda_od.arn
    depends_on = [
      aws_sqs_queue.test_queu
    ]
    definition = data.template_file.data_sf_multi_od_v3.rendered


}


//data "template_file" "state_function_multiod" {
  #
 template = stepfunctionfile
 

    //}*/


resource "aws_sfn_state_machine" "sfn_state_machine" {
  name     = "multi_test_od"
  role_arn = aws_iam_role.iam_for_lambda_od.arn

  definition = <<EOF
{
  "Comment": "",
  "StartAt": "test",
  "States": {
    "test": {
      "Type": "Task",
      "Resource": "${aws_lambda_function.test_lambda.arn}",
      "End": true
    }
  }
}
EOF



}



